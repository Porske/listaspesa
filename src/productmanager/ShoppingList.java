package productmanager;

import java.util.List;
import java.util.Optional;

/**
 * Manage a list of Product
 * 
 * @author micheleproscia
 *
 */
public interface ShoppingList {

	/**
	 * Add new Product to the list
	 * 
	 * @param product
	 * @param quantity
	 */
	void addNewProduct(final Product product);

	/**
	 * Add new Product to the list
	 * 
	 * @param name
	 * @param price
	 * @param quantity
	 */
	void addNewProduct(final String name, final double quantity, final double price);

	/**
	 * Add new Product to the list
	 * 
	 * @param name
	 * @param desc
	 * @param price
	 * @param quantity
	 */
	void addNewProduct(final String name, final Optional<String> desc, final double quantity, final double price);

	/**
	 * 
	 * @param index
	 * @return
	 */
	Optional<Product> getProductByIndex(final int index);

	/**
	 * Remove a Product that have the exact indicate name
	 * 
	 * @param name
	 */
	void removeProductByIndex(final int index);

	/**
	 * Get total price of the shopping list
	 * 
	 * @return
	 */
	double getTotal();

	/**
	 * Get the list of Product
	 * 
	 * @return
	 */
	List<Product> getList();
}
