package productmanager;

import java.util.Optional;

/**
 * Interface for shopping product
 * 
 * @author micheleproscia
 *
 */
public interface Product {

	/**
	 * 
	 * @return the product name
	 */
	String getName();

	/**
	 * 
	 * @return the product description
	 */
	Optional<String> getDesc();

	/**
	 * 
	 * @return the product quantity
	 */
	Double getQT();

	/**
	 * 
	 * @return the product price
	 */
	Double getPrice();

	/**
	 * Calculate discount of a product
	 * 
	 * @param percentage
	 * @return discount price by percentage
	 */
	Double getDiscountPrice(Double percentage);

	/**
	 * Set the product description
	 * 
	 * @param description
	 */
	void setDesc(Optional<String> description);

	/**
	 * Set the product quantity
	 * 
	 * @param quantity
	 */
	void setQT(final double quantity);

	/**
	 * Modify the product price
	 * 
	 * @param price
	 */
	void setPrice(final double price);

	/**
	 * 
	 * @return printable string for table
	 */
	String print();

}
