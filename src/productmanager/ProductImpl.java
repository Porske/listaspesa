package productmanager;

import java.util.Locale;
import java.util.Optional;

/**
 * 
 * @author micheleproscia
 *
 */
public class ProductImpl implements Product {

	/**
	 * nome del prodotto
	 */
	private final String name;
	
	/**
	 * descrizione del prodotto (non obbligatoria)
	 */
	private Optional<String> desc = Optional.empty();
	
	/**
	 * quantità del prodotto (numero unità, kg, l, ecc...)
	 */
	private double qt;
	
	/**
	 * prezzo del prodotto riguardante una singola unità
	 */
	private double price;

	/**
	 * numero massimo di caratteri dei nomi dei prodotti
	 */
	public static final int MAXCHARNAME = 15;

	/**
	 * numero massimo di caratteri per la sescrizione dei prodotti
	 */
	public static final int MAXCHARDESC = 35;

	/**
	 * Valuta dei prezzi dei prodotti
	 */
	public static final char CURRENCY = '€';

	private String cutString(final String string, final int size) {
		if (string.length() > size) {
			return string.substring(0, size);
		} else {
			return string;
		}
	}

	/**
	 * costruttore prodotto dati minimi
	 * 
	 * @param name
	 * @param quantity
	 * @param price
	 */
	public ProductImpl(final String name, final double quantity, final double price) {
		super();
		this.name = name;
		this.qt = quantity;
		this.price = price;
	}

	/**
	 * costruttore prodotto con descrizione
	 * 
	 * @param name
	 * @param desc
	 * @param quantity
	 * @param price
	 */
	public ProductImpl(final String name, final Optional<String> desc, final double quantity, final double price) {
		this(name, quantity, price);
		this.desc = desc;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Optional<String> getDesc() {
		return this.desc;
	}

	@Override
	public Double getQT() {
		return this.qt;
	}

	@Override
	public Double getPrice() {
		return this.price;
	}

	@Override
	public Double getDiscountPrice(final Double percentage) {
		return price - (price * (percentage / 100));
	}

	@Override
	public void setDesc(final Optional<String> description) {
		this.desc = description;
	}

	@Override
	public void setQT(final double quantity) {
		this.qt = quantity;
	}

	@Override
	public void setPrice(final double price) {
		this.price = price;
	}

	@Override
	public String print() {
		final String sQT = String.format(Locale.ROOT, "%.2f", this.qt);
		final String sPrice = String.format(Locale.ROOT, "%.2f", this.price);
		return String.format("%-" + MAXCHARNAME + "s", this.cutString(this.name, MAXCHARNAME)) + " |"
				+ String.format("%-" + MAXCHARDESC + "s", this.cutString(this.desc.get(), MAXCHARDESC)) + " | "
				+ String.format("%6s", sQT) + " | " + String.format("%6s", sPrice);
	}

	@Override
	public String toString() {
		return this.name + ", " + this.desc.get() + ", " + this.price;
	}
}
