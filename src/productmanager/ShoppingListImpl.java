package productmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 
 * @author micheleproscia
 *
 */
public class ShoppingListImpl implements ShoppingList {

	/**
	 * lista dei prodotti
	 */
	private final List<Product> products;

	/**
	 * Inizializzo la lista della spesa
	 */
	public ShoppingListImpl() {
		this.products = new ArrayList<>();
	}

	private boolean productExist(final String name) {
    return this.products.stream().filter(p -> p.getName().equals(name)).findFirst().isPresent();
  }

  /**
   * Ottieni prodotto tramite la posizione in lista
   * 
   * @param index
   * @return
   */
  public Optional<Product> getProductByIndex(final int index) {
    return Optional.of(this.products.get(index));
  }

  /**
   * Ottieni prodotto tramite il nome
   * 
   * @param name
   * @return
   */
  public Optional<Product> getProductByName(final String name) {
    if (this.productExist(name)) {
      return products.stream().filter(p -> p.getName().equals(name)).findFirst();
    } else {
      return Optional.empty();
    }
  }
	
	@Override
	public void addNewProduct(final Product product) {

		if (this.productExist(product.getName())) {
			products.remove(this.getProductByName(product.getName()).get());
		}

		this.products.add(product);
	}

	@Override
	public void addNewProduct(final String name, final double quantity, final double price) {
		this.addNewProduct(new ProductImpl(name, quantity, price));
	}

	@Override
	public void addNewProduct(final String name, final Optional<String> desc, final double quantity,
			final double price) {
		this.addNewProduct(new ProductImpl(name, desc, quantity, price));
	}

	@Override
	public void removeProductByIndex(final int index) {
		this.products.remove(index);
	}

	@Override
	public double getTotal() {
		return products.stream().mapToDouble(p -> p.getPrice() * p.getQT()).sum();
	}

	@Override
	public List<Product> getList() {
		return products;
	}

	@Override
	public String toString() {
		return this.products.stream().map(p -> p.print() + "\n").collect(Collectors.joining());
	}
}