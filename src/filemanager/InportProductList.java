package filemanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import productmanager.Product;
import productmanager.ProductImpl;

/**
 * Importa una lista della spesa da file e restituisce una lista di Product
 * 
 * @author micheleproscia
 *
 */
public abstract class InportProductList {

	/**
	 * Inport di un file contenente una lista di prodotti
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static List<Product> inportProducts(final File file) throws FileNotFoundException, IOException {
		final List<Product> res = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()))) {
			for (String line; (line = br.readLine()) != null;) {
				final String[] s = line.split("\\|");
				try {
					res.add(new ProductImpl(s[0], Optional.of(s[1]), Double.parseDouble(s[2]),
							Double.parseDouble(s[3])));
				} catch (Exception e) {
					Logger.getLogger(e.getMessage());
					System.out.println("Error importing file at line [ " + line + " ]");
				}
			}
		}

		return res;
	}
}
