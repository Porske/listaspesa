package filemanager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import productmanager.Product;
import productmanager.ProductImpl;

/**
 * Salva la lista di ptrodotti su file e prepara un file per essere stampato
 * 
 * @author micheleproscia
 *
 */
public abstract class SaveProductList {

	/**
	 * Salva la lista nel file indicato dall'utente
	 * 
	 * @param list
	 * @throws IOException 
	 */
	public static void dialogSave(final List<Product> list) throws IOException {
		final JFileChooser chooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		chooser.setDialogTitle("Save");
		final FileNameExtensionFilter filter = new FileNameExtensionFilter("Shopping List file", "sl");
		chooser.addChoosableFileFilter(filter);
		chooser.setFileFilter(filter);

		final int retrival = chooser.showSaveDialog(null);
		if (retrival == JFileChooser.APPROVE_OPTION) {
			final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(chooser.getSelectedFile()), "UTF-8"));
			try {
				writer.write(list.stream().map(p -> p.print() + "\n").collect(Collectors.joining()));
			} catch (Exception e) {
				//e.printStackTrace();
				Logger.getLogger(e.getMessage());
				System.err.println("Error during save " + chooser.getSelectedFile() + ".sl");
			} finally {
				writer.flush();
				writer.close();
			}
		}
	}

	/**
	 * Prepara un file ideale da essere stampato
	 * 
	 * @param list
	 * @param file
	 * @param tot
	 * @throws IOException 
	 */
	public static void savePrintFile(final List<Product> list, final File file, final double tot) throws IOException {
		System.out.println("temp save to: " + file.getAbsolutePath());

		final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		
		
		try {
			writer.write(new String(new char[ProductImpl.MAXCHARDESC + ProductImpl.MAXCHARNAME + 30]).replace("\0", "_") + "\n");
			writer.write("|Check| " + String.format("%-" + ProductImpl.MAXCHARNAME + "s", "Name") + " | " + String.format("%-" + (ProductImpl.MAXCHARDESC - 1) + "s", "Description") + " |Quantity| Price  |\n");
			writer.write("|" + new String(new char[ProductImpl.MAXCHARDESC + ProductImpl.MAXCHARNAME + 28]).replace("\0", "-") + "|\n");
			writer.write(list.stream().map(p -> "| [ ] | " + p.print() + " |\n").collect(Collectors.joining()));
			writer.write("|" + new String(new char[ProductImpl.MAXCHARDESC + ProductImpl.MAXCHARNAME + 28]).replace("\0", "-") + "|\n");
			writer.write(new String(new char[ProductImpl.MAXCHARDESC + ProductImpl.MAXCHARNAME + 16]).replace("\0", " ") + "Tot: " + ProductImpl.CURRENCY + " " + tot);

			
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger(e.getMessage());
			System.err.println("Error during save " + file.getAbsolutePath() + " for printing.");
		} finally {
			writer.flush();
			writer.close();
		}
	}
}
