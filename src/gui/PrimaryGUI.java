package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

/**
 * GUI primaria
 * 
 * @author micheleproscia
 *
 */
public class PrimaryGUI extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * GUI di partenza
	 */
	public PrimaryGUI() {
		super();
		this.setSize(400, 400);
		this.setMinimumSize(new Dimension(200, 200));
		this.setTitle("ListaSpesa");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new GridLayout(2, 1));

		final JPanel newPanel = new JPanel(new BorderLayout());
		final JPanel loadPanel = new JPanel(new BorderLayout());

		final JButton newList = new JButton("New");
		final JButton loadList = new JButton("Load");

		newPanel.setBorder(BorderFactory.createEmptyBorder(40, 20, 40, 20));
		loadPanel.setBorder(BorderFactory.createEmptyBorder(40, 20, 40, 20));

		newPanel.add(newList);
		loadPanel.add(loadList);

		this.getContentPane().add(newPanel);
		this.getContentPane().add(loadPanel);

		this.setLocationRelativeTo(null);
		this.setVisible(true);

		newList.addActionListener(e -> {
			System.out.println("New");
			new ListEdit(Optional.empty());
			dispose();
		});

		loadList.addActionListener(e -> {
			System.out.println("Load");

			final JFileChooser chooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
			chooser.setDialogTitle("Open");
			chooser.setAcceptAllFileFilterUsed(false);
			final FileNameExtensionFilter filter = new FileNameExtensionFilter("Shopping List file", "sl");
			chooser.addChoosableFileFilter(filter);

			final int returnValue = chooser.showOpenDialog(null);

			if (returnValue == JFileChooser.APPROVE_OPTION) {
				final File selectedFile = chooser.getSelectedFile();
				System.out.println(selectedFile.getAbsolutePath());
				new ListEdit(Optional.of(selectedFile));
				dispose();
			}
		});
	}
	
	/**
	 * Lancia la GUI
	 * @param args
	 */
	public static void main(final String[] args) {
    new PrimaryGUI();
  }
}
