package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Logger;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

import filemanager.InportProductList;
import filemanager.SaveProductList;
import layout.SpringUtilities;
import productmanager.Product;
import productmanager.ProductImpl;
import productmanager.ShoppingList;
import productmanager.ShoppingListImpl;

/**
 * GUI che gestisce gli elementi della lista
 * 
 * @author micheleproscia
 *
 */
public class ListEdit extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * lista della spesa
	 */
	private final ShoppingList shoppingList = new ShoppingListImpl();

	/**
	 * Model per riempire la JList
	 */
	private DefaultListModel<String> model = new DefaultListModel<>();

	/**
	 * JLable con il totale della spesa
	 */
	private final JLabel total = new JLabel("Total price : " + ProductImpl.CURRENCY + " 0.00", SwingConstants.RIGHT);

	/**
	 * Gui che gestisce graficamente la lista
	 * 
	 * @param file
	 */
	public ListEdit(final Optional<File> file) {
		super();
		this.setSize(800, 400);
		this.setMinimumSize(new Dimension(400, 200));
		this.setTitle("ListEdit");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());

		final JPanel controlPanel = new JPanel();
		final JPanel listPanel = new JPanel();
		final JPanel bottomPanel = new JPanel();

		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		listPanel.setLayout(new BorderLayout());
		bottomPanel.setLayout(new BorderLayout());

		this.getContentPane().add(controlPanel, BorderLayout.LINE_START);
		this.getContentPane().add(listPanel, BorderLayout.CENTER);
		this.getContentPane().add(bottomPanel, BorderLayout.PAGE_END);

		final JButton add = new JButton("Add");
		final JButton del = new JButton("Delete");
		final JButton edi = new JButton("Edit");
		final JButton pri = new JButton("Print");

		final JList<String> list = new JList<>(model);
		list.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14)); // Courier New
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		final JButton back = new JButton("Back");
		final JButton save = new JButton("Save");

		controlPanel.add(add);
		controlPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		controlPanel.add(del);
		controlPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		controlPanel.add(edi);
		controlPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		controlPanel.add(pri);

		controlPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		listPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		bottomPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		listPanel.add(new JScrollPane(list), BorderLayout.CENTER);
		listPanel.add(total, BorderLayout.PAGE_END);

		bottomPanel.add(back, BorderLayout.LINE_START);
		bottomPanel.add(save, BorderLayout.LINE_END);

		if (file.isPresent()) {
			final File f = file.get();
			List<Product> pl;

			try {
				pl = InportProductList.inportProducts(f);
				pl.stream().forEach(p -> shoppingList.addNewProduct(p));
			} catch (FileNotFoundException e1) {
				// e1.printStackTrace();
				Logger.getLogger(e1.getMessage());
				System.out.println("File not found.");
			} catch (IOException e2) {
				// e1.printStackTrace();
				Logger.getLogger(e2.getMessage());
				System.out.println("IO file error.");
			}

			this.updateList();
		}

		add.addActionListener(e -> {
			this.addProduct(Optional.empty());
		});

		del.addActionListener(e -> {
			if (list.getSelectedIndex() != -1) {
				this.shoppingList.removeProductByIndex(list.getSelectedIndex());
				this.updateList();
			}
		});

		edi.addActionListener(e -> {
			if (list.getSelectedIndex() != -1) {
				this.addProduct(shoppingList.getProductByIndex(list.getSelectedIndex()));
			}
		});

		pri.addActionListener(e -> {

			final File printable = new File(FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath()
					+ System.getProperty("file.separator") + "ShoppingList.txt");

			try {
				SaveProductList.savePrintFile(shoppingList.getList(), printable, shoppingList.getTotal());
			} catch (IOException e1) {
				// e1.printStackTrace();
				Logger.getLogger(e1.getMessage());
				System.err.println("Impossible save file to print");
			}

			final DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
			try {
				final Doc mydoc = new SimpleDoc(new FileInputStream(printable), flavor, null);
				final PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
				final PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, aset);
				final PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();

				if (services.length == 0) {
					if (defaultService == null) {
						System.out.println("Nessuna stampante trovata");
					} else {
						final DocPrintJob job = defaultService.createPrintJob();
						job.print(mydoc, aset);
					}
				} else {
					final PrintService service = ServiceUI.printDialog(null, 200, 200, services, defaultService, flavor,
							aset);

					if (service != null) {
						final DocPrintJob job = service.createPrintJob();
						job.print(mydoc, aset);
					}
				}
			} catch (FileNotFoundException e1) {
				// e1.printStackTrace();
				Logger.getLogger(e1.getMessage());
				System.err.println("Impossible print " + printable.getAbsolutePath() + " file not found.");
			} catch (PrintException e2) {
				// e2.printStackTrace();
				Logger.getLogger(e2.getMessage());
				System.err.println("Error during print :(");
			}

			System.out.println("Remove temp file");
			printable.delete();

		});

		back.addActionListener(e -> {
			System.out.println("Back");
			new PrimaryGUI();
			this.dispose();
		});

		save.addActionListener(e -> {
			System.out.println("Save");
			try {
				SaveProductList.dialogSave(shoppingList.getList());
			} catch (IOException e1) {
				// e1.printStackTrace();
				Logger.getLogger(e1.getMessage());
				System.err.println("Impossible save file");
			}
		});

		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	/**
	 * Apre una piccola GUI che permette l'inserimento o la modifica di un oggetto
	 * nella lista
	 * 
	 * @param product
	 */
	private void addProduct(final Optional<Product> product) {

		final JFrame productGUI = new JFrame("Product");
		productGUI.setSize(400, 800);
		productGUI.setMinimumSize(new Dimension(300, 200));
		productGUI.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		productGUI.getContentPane().setLayout(new SpringLayout());

		final JLabel lName = new JLabel("Name:");
		final JLabel lDesc = new JLabel("Description:");
		final JLabel lQT = new JLabel("Quantity:");
		final JLabel lPrice = new JLabel("Price:");
		final JLabel lDisc = new JLabel("Discount:");

		final JTextArea name = new JTextArea();
		final JTextArea desc = new JTextArea();
		final JTextArea qt = new JTextArea();
		final JTextArea price = new JTextArea();
		final JTextArea discount = new JTextArea();

		name.setLineWrap(true);
		desc.setLineWrap(true);
		qt.setLineWrap(true);
		price.setLineWrap(true);
		discount.setLineWrap(true);

		final JButton calcDis = new JButton("Calc. Discount");
		final JButton done = new JButton("Done");

		productGUI.getContentPane().add(lName);
		productGUI.getContentPane().add(name);

		productGUI.getContentPane().add(lDesc);
		productGUI.getContentPane().add(desc);

		productGUI.getContentPane().add(lQT);
		productGUI.getContentPane().add(qt);

		productGUI.getContentPane().add(lPrice);
		productGUI.getContentPane().add(price);

		productGUI.getContentPane().add(lDisc);
		productGUI.getContentPane().add(discount);

		productGUI.getContentPane().add(calcDis);
		productGUI.getContentPane().add(new JLabel());

		productGUI.getContentPane().add(new JLabel());
		productGUI.getContentPane().add(done);

		SpringUtilities.makeCompactGrid(productGUI.getContentPane(), 7, 2, 6, 6, 6, 6);

		if (product.isPresent()) {
			final Product p = product.get();
			name.setText(p.getName());
			name.setEditable(false);

			desc.setText(p.getDesc().get());
			qt.setText(p.getQT().toString());
			price.setText(p.getPrice().toString());
		}

		calcDis.addActionListener(e -> {
			try {
				final Double percentage = Double.parseDouble(discount.getText());
				final Double priceP = Double.parseDouble(price.getText());
				final Double res = priceP - (priceP * (percentage / 100));

				price.setText(res.toString());

				discount.setText("");
			} catch (Exception e2) {
				Logger.getLogger(e2.getMessage());
				System.err.println(
						"Insert correct double number for price and discount (use point separetor instead of comma es. 3.14)");
			}
		});

		done.addActionListener(e -> {

			try {

				shoppingList.addNewProduct(name.getText(), Optional.of(desc.getText()),
						Double.parseDouble(qt.getText()), Double.parseDouble(price.getText()));
				this.updateList();
				productGUI.dispose();

			} catch (Exception e2) {
				Logger.getLogger(e2.getMessage());
				System.err.println(
						"Insert correct double number for price and quantity (use point separetor instead of comma es. 3.14)");
			}
		});

		productGUI.pack();
		productGUI.setLocationRelativeTo(null);
		productGUI.setVisible(true);
	}

	/**
	 * Aggiorno la JList
	 */
	private void updateList() {
		model.removeAllElements();

		shoppingList.getList().stream().forEach(p -> model.addElement(p.print()));

		total.setText("Total price : " + ProductImpl.CURRENCY + " "
				+ String.format(Locale.ROOT, "%.2f", shoppingList.getTotal()));
	}
}
