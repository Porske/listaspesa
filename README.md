# ListaSpesa

Make simple shopping list

### Prerequisites

Java 1.8 or above

### Installing

Import in Eclipse and lauch it

### Used library

* [SpringUtilities](https://docs.oracle.com/javase/tutorial/uiswing/layout/spring.html) - Necessary for SpringLayout

## Authors

* **Proscia Michele** - *Initial work* - [Porske](https://bitbucket.org/Porske/)
